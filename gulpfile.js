const { watch, series, parallel, src, dest} = require('gulp');
const browserSync = require('browser-sync').create();
const scss = require('gulp-sass');
const plumber = require('gulp-plumber');
const notify = require('gulp-notify');
const autoprefixer = require('gulp-autoprefixer');
const sourcemaps = require('gulp-sourcemaps');
const minifyCSS = require('gulp-minify-css');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const babel = require('gulp-babel');
const htmlmin = require('gulp-htmlmin');
const spritesmith = require('gulp.spritesmith');

function sync(cb) {
    browserSync.init({
        server: {
            baseDir: "./dev"
        }
    });

    watch("src/*.html").on('change', html);
    watch('src/js/*.js').on('change', js);
    watch("src/scss/main.scss").on('change', less);
    watch('src/img/sprite/*.*').on('change', sprite);
    cb();
}


function js() {
    return src('src/js/*.js')
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(rename({ extname: '.min.js' }))
        .pipe(uglify())
        .pipe(dest('dev/js'))
        .pipe(browserSync.stream());
}

function less() {
    return src("src/scss/*.scss")
        .pipe(plumber({
            errorHandler: notify.onError(function(err){
                return {
                    title: 'Styles',
                    message: err.message
                }
            })
        }))
        .pipe(sourcemaps.init())
        .pipe(scss())
        .pipe(autoprefixer({
            browsers: ['last 3 versions'],
            cascade: false
        }))
        .pipe(minifyCSS())
        .pipe(sourcemaps.write())
        .pipe(rename({ extname: '.min.css' }))
        .pipe(dest("dev/css"))
        .pipe(browserSync.stream());
}

function html() {
    return src('src/*.html')
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(dest("dev/"))
        .pipe(browserSync.stream());
}

function sprite() {
    return src('src/img/sprite/*.*')
        .pipe(spritesmith({
            imgName: 'sprite.png',
            cssName: 'sprite.css',
        }))
        .pipe(dest('./dev/img/sprite'))
        .pipe(browserSync.stream());

}

// task list
exports.sprite = sprite;
exports.less = less;
exports.js = js;
exports.html = html;

// build and default task
exports.build = parallel(less, js, html, sprite);
exports.default = series(less, js, html, sprite, sync);



